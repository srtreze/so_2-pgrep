# pgrep
### Grade - 20
O comando pgrep é uma versão do grep que faz a pesquisa em paralelo e, de modo a simplificar, só procura palavras.

#### NOME
pgrep – procura, em paralelo, em vários ficheiros, as linhas que contêm uma palavra

#### SINOPSE
```bash
pgrep [-p n] palavra {ficheiros}
```

#### OPÇÕES
-p: ...




