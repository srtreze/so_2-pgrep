from threading import Thread, Semaphore

import sys, os, os.path, ctypes

args = sys.argv[1:]
num_processes = 0
fics = []
fic_pos = 0
busca_fic = Semaphore(1)
vou_incrementar = Semaphore(1)
vou_fazer_print = Semaphore(1)
num_linhas = 0
palavra = ""

def pesquisa_fic():

	global fic_pos
	global num_linhas
	global fics
	fich = ""
	true = True
	while true :
		
		busca_fic.acquire()
		if fic_pos < len(fics):
			fich = fics[fic_pos]
			fic_pos += 1
			busca_fic.release()

			fic = open(fich, 'r')
			linhas = []
			for linha in fic:
				if palavra in linha:

					vou_incrementar.acquire()
					num_linhas += 1
					vou_incrementar.release()

					linhas.append(fich + ": " + linha)

			vou_fazer_print.acquire()
			for linha in linhas:
				print linha
			vou_fazer_print.release()

		else:
			true = False
			busca_fic.release()


def recebe_fic():
	global fics
	more = True
	print "Insira o(s) ficheiro(s) que pretende analisar"
	print "Quando tiver incerido todos os ficheiros, insira '.exit.' numa nova linha"
	while more:
		fic_input = str(raw_input())
		if fic_input == ".exit.":
			more = False
		else:
			fic = fic_input.split()
			for f in fic:
				if os.path.isfile(f):
					fics.append(f)


if len(args) == 0:
	print "Erro: falta de argumentos"

elif len(args) == 2:

	palavra = args[0]

	if os.path.isfile(args[1]):

		fich = open(args[1], 'r')

		for linha in fich:
			if palavra in linha:
				num_linhas += 1
				print args[1] + ": " + linha
	
	else:
		print args[1] + " is not a file"

elif len(args) >= 3:

	if args[0].startswith("-"):
		if args[0] != "-p":
			print "Erro: opcao invalida"
		else:
			num_processes = int(float(args[1]))
			if num_processes >= 0:
				palavra = args[2]
				if len(args) == 3:
					recebe_fic()
				else:
					for f in args[3:]:
						if os.path.isfile(f):
							fics.append(f)
			else:
				print "Erro: numero de processos invalido"
				
	else:
		palavra = args[0]
		for f in args[1:]:
			if os.path.isfile(f):
				fics.append(f)

elif len(args) == 1 :
	palavra = args[0]
	recebe_fic()

if num_processes == 0:
	for fic in fics:

		fich = open(fic, 'r')

		for linha in fich:
			if palavra in linha:
				num_linhas += 1
				print fic + ": " + linha

else:
	t = []
	for n in range(num_processes):
		t.append(Thread(target=pesquisa_fic))
	for thread in t:
		thread.start()
	for thread in t:
		thread.join()

print "Numero de linhas: ", num_linhas
